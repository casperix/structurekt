package ru.casperix.app

import ru.casperix.app.renderer.FloatPathRenderer
import ru.casperix.app.renderer.PathInfoRenderer
import ru.casperix.app.renderer.SurfaceRenderer
import ru.casperix.demo_platform.PointBasedDemo
import ru.casperix.demo_platform.shape_editor.PointEditor
import ru.casperix.light_ui.component.ToggleGroupSelectionMode
import ru.casperix.light_ui.component.button.CheckBox
import ru.casperix.light_ui.component.button.PushButton
import ru.casperix.light_ui.component.button.ToggleButton
import ru.casperix.light_ui.component.createToggleGroup
import ru.casperix.light_ui.component.text.Label
import ru.casperix.light_ui.component.window.Window
import ru.casperix.light_ui.element.*
import ru.casperix.light_ui.layout.Layout
import ru.casperix.math.random.nextVector2f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.misc.format.FormatType
import ru.casperix.multiplatform.loader.resourceLoader
import ru.casperix.path.custom.float_path.*
import ru.casperix.renderer.Renderer2D
import kotlin.random.Random
import kotlin.time.Duration
import kotlin.time.measureTime

class GirdPathDemo(var random: Random) : PointBasedDemo() {
    private var time = Duration.ZERO
    private val cityPivots = mutableListOf<PointEditor>()

    private var pathList = listOf<FloatPath>()
    private var needRecalculate = true

    private var showAffectedTile = false
        set(value) {
            field = value
            rebuildPathInfo()
        }

    private var showOriginalTile = true
        set(value) {
            field = value
            rebuildPathInfo()
        }
    private var showActualTile = true
        set(value) {
            field = value
            rebuildPathInfo()
        }

    private var showPath = true
    private var printRoute = false
        set(value) {
            field = value
            debugSaveMap()
        }
    private var maxTryAmount = 1000
        set(value) {
            field = value
            rebuildRoute()
        }

    private var neighboursMode = NeighboursMode.DIAGONAL
        set(value) {
            field = value
            rebuildRoute()
        }
    private var randomRange: Float? = null
        set(value) {
            field = value
            rebuildAll()
        }

    private var dimension = Vector2i(128)
        set(value) {
            field = value
            rebuildAll()
        }

    private var mapType = SurfaceGeneratorType.NOISE
        set(value) {
            field = value
            rebuildAll()
        }

    private var map: SurfaceMap = SurfaceMapBuilder(dimension, mapType, randomRange)
    private var surfaceRenderer = SurfaceRenderer(map)
    private val pathInfoRenderers = mutableListOf<PathInfoRenderer>()
    private val pathRenderers = mutableListOf<FloatPathRenderer>()

    private val txtInfo = Label()

    override val root = Window(
        "float path", Layout.VERTICAL,
        listOf(
            Window(
                "cities", Layout.HORIZONTAL,
                PushButton("add") { addCity() },
                PushButton("remove") { removeCity() },
            ),
            Window(
                "view", Layout.VERTICAL,
                CheckBox("show affected tiles", showAffectedTile) {
                    showAffectedTile = isChecked
                },
                CheckBox("show original path tiles", showOriginalTile) {
                    showOriginalTile = isChecked
                },
                CheckBox("show actual path tiles", showActualTile) {
                    showActualTile = isChecked
                },
                CheckBox("show path", showPath) {
                    showPath = isChecked
                },
                CheckBox("print route", printRoute) {
                    printRoute = isChecked
                },
            ),
            Window(
                "map size", Layout.HORIZONTAL,
                createToggleGroup(
                    Layout.HORIZONTAL, ToggleGroupSelectionMode.ALWAYS_ONE,
                    ToggleButton("128x128", false) {
                        if (!isChecked) return@ToggleButton
                        dimension = Vector2i(128)
                    },
                    ToggleButton("256x256", false) {
                        if (!isChecked) return@ToggleButton
                        dimension = Vector2i(256)
                    },
                    ToggleButton("512x512", false) {
                        if (!isChecked) return@ToggleButton
                        dimension = Vector2i(512)
                    },
                    ToggleButton("1024x1024", false) {
                        if (!isChecked) return@ToggleButton
                        dimension = Vector2i(1024)
                    },
                )
            ),
            Window(
                "map type", Layout.HORIZONTAL,
                createToggleGroup(
                    Layout.HORIZONTAL, ToggleGroupSelectionMode.ALWAYS_ONE,
                    ToggleButton("flat", false) {
                        if (!isChecked) return@ToggleButton
                        mapType = SurfaceGeneratorType.FLAT
                    },
                    ToggleButton("noise", true) {
                        if (!isChecked) return@ToggleButton
                        mapType = SurfaceGeneratorType.NOISE
                    },
                )
            ),
            Window(
                "noise", Layout.HORIZONTAL,
                createToggleGroup(
                    Layout.HORIZONTAL, ToggleGroupSelectionMode.ALWAYS_ONE,
                    ToggleButton("off", true) {
                        if (!isChecked) return@ToggleButton
                        randomRange = null
                    },
                    ToggleButton("low", false) {
                        if (!isChecked) return@ToggleButton
                        randomRange = 0.5f
                    },
                    ToggleButton("medium", false) {
                        if (!isChecked) return@ToggleButton
                        randomRange = 1f
                    },
                    ToggleButton("high", false) {
                        if (!isChecked) return@ToggleButton
                        randomRange = 2f
                    },
                )
            ),
            Window(
                "neighbours", Layout.HORIZONTAL,
                createToggleGroup(
                    Layout.HORIZONTAL, ToggleGroupSelectionMode.ALWAYS_ONE,
                    ToggleButton("orthogonal", false) {
                        if (!isChecked) return@ToggleButton
                        neighboursMode = NeighboursMode.ORTHOGONAL
                    },
                    ToggleButton("diagonal", true) {
                        if (!isChecked) return@ToggleButton
                        neighboursMode = NeighboursMode.DIAGONAL
                    },
                    ToggleButton("ext2", false) {
                        if (!isChecked) return@ToggleButton
                        neighboursMode = NeighboursMode.EXTENDED2
                    },
                    ToggleButton("ext3", false) {
                        if (!isChecked) return@ToggleButton
                        neighboursMode = NeighboursMode.EXTENDED3
                    },
                )
            ),
            Window(
                "tight steps", Layout.HORIZONTAL,
                createToggleGroup(
                    Layout.HORIZONTAL, ToggleGroupSelectionMode.ALWAYS_ONE,
                    ToggleButton("1", false) {
                        if (!isChecked) return@ToggleButton
                        maxTryAmount = 1
                    },
                    ToggleButton("10", false) {
                        if (!isChecked) return@ToggleButton
                        maxTryAmount = 10
                    },
                    ToggleButton("100", false) {
                        if (!isChecked) return@ToggleButton
                        maxTryAmount = 100
                    },
                    ToggleButton("1000", true) {
                        if (!isChecked) return@ToggleButton
                        maxTryAmount = 1000
                    },
                    ToggleButton("10000", false) {
                        if (!isChecked) return@ToggleButton
                        maxTryAmount = 10000
                    },
                )
            ),
            Window(
                "info", Layout.HORIZONTAL,
                txtInfo.apply {
                    placement.sizeMode = SizeMode(MaxContentOrViewDimension, ContentDimension)
                },
            ),
        )
    )

    init {
        cityPivots += PointEditor(pointController, Vector2f(64f, 32f))
        cityPivots += PointEditor(pointController, Vector2f(8f))

        pointController.pointDragged.then {
            rebuildRoute()
        }
        rebuildRoute()
    }

    override fun render(renderer: Renderer2D) {
        update()

        surfaceRenderer.render(renderer)

        pathInfoRenderers.forEach {
            it.render(renderer)
        }

        if (showPath) {
            pathRenderers.forEach {
                it.render(renderer)
            }
        }

        super.render(renderer)
    }

    private fun rebuildAll() {
        map = SurfaceMapBuilder(dimension, mapType, randomRange)
        surfaceRenderer = SurfaceRenderer(map)
        debugSaveMap()
        rebuildRoute()
    }

    private fun debugSaveMap() {
        if (printRoute) {
            if (map.dimension.greater(Vector2i.ZERO)) {
                resourceLoader.saveImage("map.png", surfaceRenderer.texture.map)
            }
        }
    }

    private fun addCity() {
        val start = random.nextVector2f(Vector2f.ZERO, dimension.toVector2f())
        cityPivots += PointEditor(pointController, start)
        rebuildRoute()
    }

    private fun removeCity() {
        cityPivots.removeLastOrNull()?.dispose()
        rebuildRoute()
    }


    private fun rebuildRoute() {
        needRecalculate = true
    }

    private fun update() {
        if (!needRecalculate) {
            return
        }
        needRecalculate = false

        random = Random(1)
        time = measureTime {
            generateCityMesh()
            if (printRoute) {
                printPathList()
            }
        }

        txtInfo.text = getInfo()
    }

    private fun getInfo(): String {

        val points = pathList.sumOf {
            it.pathPoints.size
        }
        val affected = pathList.sumOf {
            it.affectedTiles.size
        }

        return listOf(
            "Generated for ${time.inWholeMilliseconds}ms",
            "Affected: $affected",
            "Points: $points",
        ).joinToString("\n")
    }

    private fun printPathList() {
        pathInfoRenderers.forEachIndexed { index, path ->
            val output = path.originalTiles.map { point ->
                point.format(FormatType.SHORT)
            }.joinToString(";")

            println("${index + 1}. $output")
        }
    }

    private fun generateCityMesh() {
        val pathFactory = FloatPathFactory(MapInfo(map, mutableMapOf()), neighboursMode, maxTryAmount)
        val pathInfoList = cityPivots.indices.flatMap { s ->
            cityPivots.indices.flatMap { t ->
                if (s > t) {
                    val A = cityPivots.get(s).getPoints().first().position.toVector2i()
                    val B = cityPivots.get(t).getPoints().first().position.toVector2i()
                    pathFactory.generatePath(A, B)
                } else {
                    emptyList()
                }
            }
        }

        this.pathList = pathInfoList

        rebuildPathInfo()
    }

    class FloatPathBuffer(val affectedTiles: Collection<Vector2i>, val originalTiles: Collection<Vector2i>, val actualTiles: Collection<Vector2i>)

    private fun rebuildPathInfo() {
        pathRenderers.clear()
        pathList.forEach { pathInfo ->
            pathRenderers += FloatPathRenderer(pathInfo.pathPoints)
        }

        pathInfoRenderers.clear()
        val buffer = FloatPathBuffer(
            pathList.flatMap { it.affectedTiles },
            pathList.flatMap { it.originalTiles },
            pathList.flatMap { it.pathTiles.keys },
        )

        pathInfoRenderers += PathInfoRenderer(
            dimension.toDimension2i(),
            if (showAffectedTile) buffer.affectedTiles else emptyList(),
            if (showOriginalTile) buffer.originalTiles else emptyList(),
            if (showActualTile) buffer.actualTiles else emptyList(),
        )
    }
}