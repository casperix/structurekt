package ru.casperix.app.renderer

import ru.casperix.math.axis_aligned.float32.Box2f
import ru.casperix.math.axis_aligned.int32.Dimension2i
import ru.casperix.math.color.Color
import ru.casperix.math.color.Colors
import ru.casperix.math.quad_matrix.float32.Matrix3f
import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.int32.Vector2i
import ru.casperix.math.vector.toQuad
import ru.casperix.renderer.Renderer2D
import ru.casperix.renderer.material.SimpleMaterial
import ru.casperix.renderer.material.Texture2D
import ru.casperix.renderer.material.TextureConfig
import ru.casperix.renderer.material.TextureFilter
import ru.casperix.renderer.pixel_map.PixelMap
import ru.casperix.renderer.vector.vertex.Vertex
import ru.casperix.renderer.vector.vertex.VertexPosition2
import ru.casperix.renderer.vector.vertex.VertexTextureCoord

class TileGroupsRenderer(
    val dimension: Dimension2i,
    groups: List<TileGroup>,
) {
    class TileGroup(val color:Color, val tiles:Collection<Vector2i>)

    val texture: Texture2D

    init {
        val pixelMap = PixelMap.RGBA(dimension)
        val raw = pixelMap.RGBA()!!

        groups.forEach { group->
            val color = group.color.toColor4b()
            group.tiles.forEach {
                raw.set(it, color)
            }
        }

        texture = Texture2D(pixelMap, TextureConfig(TextureFilter.LINEAR, TextureFilter.NEAREST))
    }

    fun render(renderer: Renderer2D) {
        renderer.drawQuad(SimpleMaterial(texture), Box2f.byDimension(Vector2f.ZERO, dimension.toVector2f()).toQuad().convert {
            Vertex(VertexPosition2(it), VertexTextureCoord(it.sign))
        }, Matrix3f.IDENTITY)
    }
}