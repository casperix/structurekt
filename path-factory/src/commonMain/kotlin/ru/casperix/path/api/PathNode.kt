package ru.casperix.path.api

class PathNode<Node>(val previous:Node?, val current: Node?, val weight: Int)