package ru.casperix.path.custom.city_road_generator

import ru.casperix.math.angle.float32.DegreeFloat
import ru.casperix.math.polar.float32.PolarCoordinateFloat
import kotlin.random.Random


interface EdgeGenerator<Marker:Any> {
    fun getDist(random: Random, point: Point<Marker>): Float
    fun getAngle(random: Random, builder: MeshBuilder<Marker>, point: Point<Marker>): DegreeFloat

    fun nextEdge(random: Random, builder: MeshBuilder<Marker>, point: Point<Marker>): PolarCoordinateFloat {
        val nextAngle = getAngle(random, builder, point)
        val nextDist = getDist(random, point)
        return PolarCoordinateFloat(nextDist, nextAngle.toRadian())
    }
}