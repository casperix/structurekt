package ru.casperix.path.custom.city_road_generator

import ru.casperix.math.straight_line.float32.LineSegment2f
import ru.casperix.math.vector.float32.Vector2f


class MeshBuilder<Marker:Any>(val checker: MeshChecker<Marker>) {
    val points = mutableMapOf<Vector2f, Point<Marker>>()
    val edges = mutableMapOf<Edge, Marker>()


    fun nearPoint(point: Vector2f, maxDist: Float): Vector2f {
        return points.keys.filter { it.distTo(point) <= maxDist }.minByOrNull { it.distTo(point) } ?: point
    }

    fun getPointOrDefault(position: Vector2f, marker:Marker): Point<Marker> {
        return points[position] ?: Point(position, marker, emptySet())
    }

    fun addEdgeIfValid(connection: LineSegment2f, marker: Marker): Boolean {
        val edge = Edge(connection)

        if (!checker.isAcceptable(edge)) {
            return false
        }

        edges.keys.forEach {
            if (!checker.isAcceptable(it, edge)) {
                return false
            }
        }

        val start = connection.start
        val finish = connection.finish

        val startPoint = getPointOrDefault(start, marker).addConnection(finish)
        if (!checker.isAcceptable(startPoint)) {
            return false
        }

        val finishPoint = getPointOrDefault(finish, marker).addConnection(start)
        if (!checker.isAcceptable(finishPoint)) {
            return false
        }
        val lastMarker = edges[edge]
        if (lastMarker != null && !checker.canReplace(lastMarker, marker)) {
            return false
        }

        addEdge(edge, marker)
        return true
    }

    fun addEdge(edge: Edge, marker:Marker) {
        val start = edge.connection.start
        val finish = edge.connection.finish

        val startPoint = getPointOrDefault(start, marker)
        val finishPoint = getPointOrDefault(finish, marker)

        edges[edge] = marker
        points[start] = startPoint.addConnection(finish)
        points[finish] = finishPoint.addConnection(start)
    }
}