package ru.casperix.path.custom.city_road_generator

interface MeshChecker<Marker:Any> {
    fun canReplace(lastMarker:Marker, nextMarker: Marker): Boolean
    fun isAcceptable(point: Point<Marker>): Boolean
    fun isAcceptable(edge: Edge): Boolean
    fun isAcceptable(a: Edge, b: Edge): Boolean
}