package ru.casperix.path.custom.float_path

import ru.casperix.math.array.Map2D


interface SurfaceMap : Map2D<Tile>

