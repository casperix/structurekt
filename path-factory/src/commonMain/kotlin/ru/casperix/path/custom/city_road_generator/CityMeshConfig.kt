package ru.casperix.path.custom.city_road_generator

import ru.casperix.math.angle.float32.DegreeFloat

class CityMeshConfig(
    val basisLevel: Int = 4,
    val maxConnections: Int = 5,

    val basisDist: Float = 1f,
    val deltaDist: Float = 0.5f,
    val deltaAngle: Float = 10f,

    val minLength: Float = 0.3f,
    val closestDist: Float = 0.5f,
    val minAngle: DegreeFloat = DegreeFloat(45f),

    val magnetFactor: Float = 0.3f,
    val magnetOffset: Float = 1f,
)