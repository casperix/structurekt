package ru.casperix.path.custom

import ru.casperix.path.api.NodeEstimator
import ru.casperix.path.api.PathNode
import ru.casperix.path.api.Path
import ru.casperix.misc.collection.PriorityQueue
import kotlin.math.max

class PathFactory<Node>(
    val estimator: NodeEstimator<Node>,
    val start: Node,
    var maxSteps: Int = Int.MAX_VALUE,
    var maxWeight: Int = Int.MAX_VALUE,
    val initialCapacity: Int = 32,
) {

    val searchMap: Map<Node, PathNode<Node>>
    var searchSteps = 0
    var searchRecursiveSteps = 0

    val path: Path<Node>?
    var weight = 0
    var maxRecursiveSteps = Int.MAX_VALUE

    init {

        if (estimator.isFinish(null, start)) {
            searchMap = emptyMap()
            path = Path(listOf(start), 0)
        } else {
            val result = buildPathMap(start, initialCapacity)
            if (result != null) {
                val (finish, pathMap) = result
                searchMap = pathMap
                path = createPath(start, finish, pathMap)
            } else {
                searchMap = emptyMap()
                path = null
            }
        }
    }


    private fun buildPathMap(start: Node, initialCapacity: Int): Pair<Node, MutableMap<Node, PathNode<Node>>>? {
        val frontier = PriorityQueue<PathNode<Node>> { a, b -> a.weight - b.weight }
        frontier.add(PathNode(null, start, 0))

        //	key:	next edge
        //	value: last edge; weight from "finish edge" to "next edge"
        val cameFrom = HashMap<Node, PathNode<Node>>(initialCapacity, 0.5f)
        cameFrom[start] = PathNode<Node>(start, null, 0)

        //	move step by step from finish to start
        while (!frontier.isEmpty()) {
            if (++searchSteps > maxSteps) break

            val current = frontier.poll()
            val currentEdge = current.current ?: throw Exception("Error")
            val edgeNode = cameFrom[currentEdge]!!

            if (estimator.isFinish(edgeNode.current, currentEdge)) {
                //	We connected the beginning and the end!
                return Pair(currentEdge, cameFrom)
            }
            val currentWeight = edgeNode.weight
            val neighbours = estimator.getNeighbours(currentEdge)
            for (nextEdge in neighbours) {
                if (++searchRecursiveSteps > maxRecursiveSteps) break

                val nodeWeight = estimator.getWeight(edgeNode.previous, currentEdge, nextEdge) ?: continue
                val nextWeight = currentWeight + nodeWeight

                val lastWeight = cameFrom[nextEdge]?.weight
                if (lastWeight == null || nextWeight < lastWeight) {
                    if (nextWeight > maxWeight) continue
                    if (nextWeight < 0) throw Error("Weight must be not negative: $nextWeight")
                    weight = max(weight, nextWeight)

                    frontier.add(PathNode(currentEdge, nextEdge, nextWeight + estimator.getHeuristic(nextEdge)))
                    cameFrom[nextEdge] = PathNode(null, currentEdge, nextWeight)
                }
            }
        }
        //	path not exist
        return null
    }

    private fun createPath(start: Node, finish: Node, map: MutableMap<Node, PathNode<Node>>): Path<Node> {
        val nodes = mutableListOf<Node>()

        var rightNode = finish

        do {
            nodes.add(0, rightNode)

            val leftNode = map[rightNode] ?: throw Exception("Invalid cameFrom map")
            leftNode.current ?: throw Exception("Invalid node")

            rightNode = leftNode.current

        } while (rightNode != start)
        nodes.add(0, rightNode)

        val weight = map[finish]!!.weight

        return Path(nodes, weight)
    }
}