package ru.casperix.path.custom.float_path

import ru.casperix.math.vector.float32.Vector2f
import ru.casperix.math.vector.int32.Vector2i

class MapInfo(val map: SurfaceMap, val roadMap: MutableMap<Vector2i, Vector2f>) {
    val roads get() = roadMap.keys

    fun nearRoad(target: Vector2f): Vector2f? {
        val pivot = target.toVector2i()
        val nearTiles = (-1..1).flatMap { x ->
            (-1..1).mapNotNull { y ->
                roadMap[pivot + Vector2i(x, y)]
            }
        }

        return nearTiles.minByOrNull { it.distTo(target) }
    }

}