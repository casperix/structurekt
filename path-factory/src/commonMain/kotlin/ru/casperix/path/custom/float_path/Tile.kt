package ru.casperix.path.custom.float_path

data class Tile(val complexity: Float) {
    companion object {
        val WATER = Tile(Float.POSITIVE_INFINITY)
        val GRASS = Tile(4f)
        val ROCK = Tile(16f)
    }
}
